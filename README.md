# Практическая работа по модулю "Виртуализация" курса "DevOps-инженер. Основы"

1. Устанавливаем VirtualBox и vagrant

1. Клонируем репозитории
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/basic/module_7.git
   cd module_7
   git clone https://gitlab.com/entsupml/skillbox-deploy-blue-green.git ../skillbox-deploy-blue-green/
   ```
   
1. Запускаем vagrant
   ```shell
   vagrant up --provision
   ```
   
1. Ждем окончания инициализации и настройки ВМ

1. Заходим на ВМ по SSH
   ```shell
   vagrant ssh
   ```
   
1. Переходим в папку с react-приложением и запускаем сервер
   ```shell
   cd /var/www/releases/local && yarn start
   ```
   
1. На локальной машине переходим в браузере по пути [http://10.10.10.10:3000/](http://10.10.10.10:3000/). Должны увидеть запущенное react-приложение.
   ![Работающее на ВМ react-приложение](imgs/vagrant-react-app.png)
